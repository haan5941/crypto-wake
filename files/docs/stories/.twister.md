# Twister

Twister was a peer-to-peer microblogging service that used technology based on Bitcoin and BitTorrent. It first appeared in late 2013 and has not released a new version since 2018.

## Lore

## Lessons

## Links

* [Website](http://twister.net.co/)
* [Wikipedia page](https://en.wikipedia.org/wiki/Twister_(software))
* [Codebase](https://github.com/miguelfreitas/twister-core)
