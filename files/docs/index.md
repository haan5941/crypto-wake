# Welcome

*An archive of inactive, failed, and otherwise deceased crypto projects with lessons worth remembering.*

<div style='color:gray; padding:15px;'>
<span style='font-size:x-large'>A wake is a social gathering associated with death, held before a funeral. Traditionally, a wake involves family and friends keeping watch over the body of the dead person, usually in the home of the deceased. … The wake or the viewing of the body is a prominent part of death rituals in many cultures. It allows one last interaction with the dead, providing a time for the living to express their thoughts and feelings with the deceased. It highlights the idea that the loss is one of a social group affecting that group as a whole, and is a way of honoring the dead.</span> —<a href="https://en.wikipedia.org/wiki/Wake_(ceremony)">Wikipedia</a>
</div>

Please consider [contributing](about/contributing).
