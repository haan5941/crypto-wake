# Basics

Crypto moves quickly. Projects come and go. In markets full of volatility and speculation, success or failure is not always the clearest indicator of the quality of the underlying ideas. Important ideas may appear in a given project alongside the lousy ones that doom it. Lousy ideas, also, may seem to flourish because they appeared at an opportune time. Regardless of the quality of an idea, it can bear lessons.

The culture that dominates in crypto tends to focus on the future, as if that is all that matters. Memory is short. But there is much to be learned for the future from even the short history of crypto experiments. Crypto Wake is a platform for doing that learning together.

Crypto Wake is an archive of inactive, failed, and otherwise deceased crypto projects with lessons worth remembering. It is a collaborative wiki that welcomes contributions from those who remember or who are learning to do so.

This project is the result of a quick conversation between Ellie Rennie and Nathan Schneider at Consensus 2022. The initial pilot is being developed by the Media Enterprise Design Lab at the University of Colorado with support from the Smart Contract Research Forum.
